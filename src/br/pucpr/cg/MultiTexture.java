package br.pucpr.cg;

import static org.lwjgl.glfw.GLFW.*;
import static org.lwjgl.opengl.GL11.*;

import java.io.File;
import java.io.IOException;

import br.pucpr.mage.*;
import br.pucpr.mage.phong.PhongMaterial;
import org.joml.Matrix4f;
import org.joml.Vector2f;
import org.joml.Vector3f;

import br.pucpr.mage.phong.DirectionalLight;
import br.pucpr.mage.phong.MultiTextureMaterial;
import br.pucpr.mage.phong.SkyMaterial;
import br.pucpr.mage.postfx.PostFXMaterial;

public class MultiTexture implements Scene {
    private static final String PATH = "c:/temp/img/opengl/";
    
    private Keyboard keys = Keyboard.getInstance();
    
    //Dados da cena
    private GoodCamera camera = new GoodCamera();
    private DirectionalLight light;

    //Dados da malha
    private Mesh mesh;
    private MultiTextureMaterial material; 

    private PhongMaterial texTest;

    private Mesh skydome;
    private SkyMaterial skyMaterial;    

    private Mesh canvas;
    private FrameBuffer fb;
    private PostFXMaterial postFX;

    private float cloud0Offset = -0.001f;
    private float cloud1Offset = 0.002f;

    private float time = 0.0f;

    @Override
    public void init() {
        glEnable(GL_DEPTH_TEST);
        glEnable(GL_CULL_FACE);
        glPolygonMode(GL_FRONT_FACE, GL_LINE);
        glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
        
        camera.getPosition().set(0.0f, 50.0f, 145.0f);
        camera.rotate(185);
        camera.rotateX(0.5f);

        light = new DirectionalLight(
                new Vector3f( 1.0f, -1.0f, -1.0f),    //direction
                new Vector3f( 0.1f,  0.1f,  0.1f), //ambient
                new Vector3f( 1.0f,  1.0f,  1.0f),    //diffuse
                new Vector3f( 1.0f,  1.0f,  1.0f));   //specular

        try {
            mesh = MeshFactory.loadTerrain(new File("resources/volcano.png"), 0.4f, 1);
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            System.exit(1);
        }
        
        
        material = new MultiTextureMaterial(
                new Vector3f(1.0f, 1.0f, 1.0f), //ambient
                new Vector3f(0.9f, 0.9f, 0.9f), //diffuse
                new Vector3f(0.0f, 0.0f, 0.0f), //specular
                0.0f);                          //specular power

        texTest = new PhongMaterial(
                new Vector3f(1.0f, 1.0f, 1.0f), //ambient
                new Vector3f(0.9f, 0.9f, 0.9f), //diffuse
                new Vector3f(0.0f, 0.0f, 0.0f), //specular
                0.0f);                          //specular power

        try {
            material.setTexture("uBlendMap", BlendMapCreator.createBlendMap("resources/volcano.png"));
        } catch (IOException e) {
            e.printStackTrace();
        }

        material.setTextures(
        		new Texture("resources/sand.png"),
        		new Texture("resources/grass.png"),
        		new Texture("resources/rock.png"),
        		new Texture("resources/snow.png"));

        texTest.setTexture(new Texture("resources/test.png"));

        canvas = MeshFactory.createCanvas();
        fb = FrameBuffer.forCurrentViewport();
        postFX = PostFXMaterial.defaultPostFX("fxNone", fb);
        
        skydome = MeshFactory.createSphere(20, 20);
        skyMaterial = new SkyMaterial();

        skyMaterial.setCloud(0, new Texture("resources/cloud1.png"));
        skyMaterial.setCloud(1, new Texture("resources/cloud2.png"));
    }

    @Override
    public void update(float secs) {
        float SPEED = 10;

        if (keys.isPressed(GLFW_KEY_ESCAPE)) {
            glfwSetWindowShouldClose(glfwGetCurrentContext(), GLFW_TRUE);
            return;
        }
        
        if (keys.isDown(GLFW_KEY_LEFT_SHIFT)) {
            SPEED *= 10;
        }
        
        if (keys.isDown(GLFW_KEY_LEFT)) {
            camera.rotate(1 * secs);
        } else if (keys.isDown(GLFW_KEY_RIGHT)) {
            camera.rotate(-1 * secs);
        }
        
        if (keys.isDown(GLFW_KEY_UP)) {
            camera.rotateX(-1 * secs);
        } else if (keys.isDown(GLFW_KEY_DOWN)) {
            camera.rotateX(1 * secs);
        }

        if(keys.isDown(GLFW_KEY_W)){
            camera.move(SPEED, secs);
        } else if(keys.isDown(GLFW_KEY_S)){
            camera.move(-SPEED, secs);
        }

        if(keys.isDown(GLFW_KEY_A)){
            camera.strafe(SPEED, secs);
        } else if(keys.isDown(GLFW_KEY_D)){
            camera.strafe(-SPEED, secs);
        }

        time += secs;

        skyMaterial.getShader()
                .bind()
                .setUniform("uCloud0Offset", new Vector2f(cloud0Offset).mul(time))
                .setUniform("uCloud1Offset", new Vector2f(cloud1Offset).mul(time))
                .unbind();

    }

    public void drawSky() {
        glDisable(GL_DEPTH_TEST);    
        Shader shader = skyMaterial.getShader();
        shader.bind()
            .setUniform("uProjection", camera.getProjectionMatrix())
            .setUniform("uView", camera.getViewMatrix())            
        .unbind();
                
        skydome.setUniform("uWorld", new Matrix4f().scale(300));
        skydome.draw(skyMaterial);
        glEnable(GL_DEPTH_TEST);
    }
    
    public void drawScene(Material mat) {
        Shader shader = mat.getShader();
        shader.bind()
            .setUniform("uProjection", camera.getProjectionMatrix())
            .setUniform("uView", camera.getViewMatrix())
            .setUniform("uCameraPosition", camera.getPosition());        
        light.apply(shader);        
        shader.unbind();
    
        mesh.setUniform("uWorld", new Matrix4f().rotateY((float)Math.toRadians(85)));
        mesh.draw(mat);
    }
    
    @Override
    public void draw() {        
        fb.bind();
	        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	        drawSky();
	        drawScene(material);
        fb.unbind();
        
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);        
        canvas.draw(postFX);
    }

    @Override
    public void deinit() {
    }

    public static void main(String[] args) {
        new Window(new MultiTexture(), "Multi texturing", 1024, 748).show();
    }
}
