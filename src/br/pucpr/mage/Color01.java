package br.pucpr.mage;

import java.awt.*;

public class Color01 {
    public static final Color01 white = new Color01(1, 1, 1);
    public static final Color01 black = new Color01(0, 0, 0);

    public float r;
    public float g;
    public float b;
    public float a;

    public Color01(float r, float g, float b, float a) {
        this.r = r;
        this.g = g;
        this.b = b;
        this.a = a;
    }

    public Color01(float r, float g, float b) {
        this.r = r;
        this.g = g;
        this.b = b;
        this.a = 1;
    }

    public Color01(int color) {
        Color temp = new Color(color);

        r = MathHelper.clamp0255(temp.getRed());
        g = MathHelper.clamp0255(temp.getGreen());
        b = MathHelper.clamp0255(temp.getBlue());
        a = MathHelper.clamp0255(temp.getAlpha());

        r = MathHelper.clamp01(r / 255f);
        g = MathHelper.clamp01(g / 255f);
        b = MathHelper.clamp01(b / 255f);
        a = MathHelper.clamp01(a / 255f);
    }

    public int toIntColor() {
        return new Color(r, g, b, a).getRGB();
    }
}