package br.pucpr.mage;

public class MathHelper {
    public static float lerp(float a, float b, float t) {
        return a + (b - a) * clamp01(t);
    }

    public static float clamp01(float n) {
        if (n < 0) return 0;
        else if (n > 1) return 1;
        else return n;
    }

    public static int clamp0255(int n) {
        if (n < 0) return 0;
        else if (n > 255) return 255;
        else return n;
    }

    public static float clamp(float n, float min, float max) {
        if (n < min) return min;
        else if (n > max) return max;
        else return n;
    }
}