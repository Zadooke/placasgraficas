package br.pucpr.mage;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.nio.file.Paths;

public class BlendMapCreator {
    public static Texture createBlendMap(String path) throws IOException {

        BufferedImage heightmap = ImageIO.read(new File(path));
        BufferedImage blendmap = new BufferedImage(heightmap.getWidth(), heightmap.getHeight(), BufferedImage.TYPE_INT_RGB);


        for (int y = 0; y < heightmap.getHeight(); y++) {
            for (int x = 0; x < heightmap.getWidth(); x++) {
                float grayscale = new Color01(heightmap.getRGB(x, y)).r;

                float a = MathHelper.lerp(1, 0, grayscale * 3);
                float c = MathHelper.lerp(0, 1, grayscale - 0.5f) * 2;
                float b = 1 - a - c;

                a = MathHelper.clamp01(a);
                b = MathHelper.clamp01(b);
                c = MathHelper.clamp01(c);

                Color blendColor = new Color(a, b, c, 1);

                blendmap.setRGB(x, y, blendColor.getRGB());
            }
        }

        ImageIO.write(blendmap, "png", new File("processed/" + "blendmap" + "." + "png"));

        return new Texture("processed/blendmap.png");
    }

}
