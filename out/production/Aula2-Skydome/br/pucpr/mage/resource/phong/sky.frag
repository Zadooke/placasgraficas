#version 330

uniform sampler2D uCloud0;
uniform sampler2D uCloud1;

uniform vec2 uCloud0Offset;
uniform vec2 uCloud1Offset;

in float vY;
in vec2 vTexCoord;

out vec4 outColor;

void main() {

    vec3 colorLow = vec3(0.758, 0.986, 1.00);
    vec3 colorHigh = vec3(0.435, 0.353, 0.725);

    vec3 color = mix(colorLow, colorHigh, vY);

    vec3 cloud1Tex = texture(uCloud0, (vTexCoord + uCloud0Offset) * 2).xyz;
    vec3 cloud2Tex = texture(uCloud1, (vTexCoord + uCloud1Offset) * 2).xyz;

    color = clamp(color + cloud1Tex + cloud2Tex, 0.0, 1.0);

    outColor = vec4(color, 1.00);
}