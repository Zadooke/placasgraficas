#version 330

uniform vec3 uLightDir;

uniform vec3 uAmbientLight;
uniform vec3 uDiffuseLight;
uniform vec3 uSpecularLight;

uniform vec3 uAmbientMaterial;
uniform vec3 uDiffuseMaterial;
uniform vec3 uSpecularMaterial;

uniform float uSpecularPower;

uniform sampler2D uBlendMap;

uniform sampler2D uTexture1;
uniform sampler2D uTexture2;
uniform sampler2D uTexture3;
uniform sampler2D uTexture4;

in vec3 vNormal;
in vec3 vViewPath;
in vec2 vTexCoord;

in float vDepth;
in vec4 vTexWeight;

out vec4 outColor;

void main() {

    vec3 L = normalize(uLightDir);
	vec3 N = normalize(vNormal);

    vec3 ambient = uAmbientLight * uAmbientMaterial;
    
    float diffuseIntensity = max(dot(N, -L), 0.0);
    vec3 diffuse = diffuseIntensity * uDiffuseLight * uDiffuseMaterial;
       
    //Calculo do componente especular
	float specularIntensity = 0.0;
	if (uSpecularPower > 0.0) {
		vec3 V = normalize(vViewPath);
		vec3 R = reflect(L, N);
		specularIntensity = pow(max(dot(R, V), 0.0), uSpecularPower);
	}
    vec3 specular = specularIntensity * uSpecularLight * uSpecularMaterial;
    
    //Calculo de mixagem das texturas de perto e longe
    float blendDistance = 0.99;
    float blendWidth = 100;
    float blendFactor = clamp((vDepth - blendDistance) * blendWidth, 0.0, 1.0);

    // Pega valores da imagem de blend map, na coordenada atual
    vec4 blendMap = texture(uBlendMap, vTexCoord); // RGBA

    /*
        Areia é a textura base. Todas as outras são colocadas por cima

        r - grama
        g - rocha
        b - neve
    */

    /*
        Transformar os valores de R, G e B de 0 a 1, dependendo da presença deles na cor atual
        Regra de 3 para cada canal
    */
    float rAmount = blendMap.r / (blendMap.r + blendMap.g + blendMap.b);
    float gAmount = blendMap.g / (blendMap.r + blendMap.g + blendMap.b);
    float bAmount = blendMap.b / (blendMap.r + blendMap.g + blendMap.b);

    // Calcula a quantidade de areia no mapa, essa vai ser o resto do mapa, aonde as outras texturas não se aplicam
    float sandAmount = 1 - (rAmount + bAmount + gAmount);

    // Texel para curtas distancias, mais repetições de textura
    vec2 nearCoord = vTexCoord * 50.0;
    vec4 texelNear =
    	texture(uTexture1, nearCoord) * sandAmount +    // Areia, textura base
    	texture(uTexture2, nearCoord) * blendMap.r +    // Grama
    	texture(uTexture3, nearCoord) * blendMap.g +    // Rocha
        texture(uTexture4, nearCoord) * blendMap.b;     // Neve

    // Texel para longas distancias, menores repetições de textura
    vec2 farCoord = vTexCoord * 10.0;
    vec4 texelFar = 
    	texture(uTexture1, farCoord) * sandAmount +    // Areia, textura base
    	texture(uTexture2, farCoord) * blendMap.r +    // Grama
    	texture(uTexture3, farCoord) * blendMap.g +    // Rocha
        texture(uTexture4, farCoord) * blendMap.b;     // Neve

    // Mistura os dois Texels para transição entre perto e longe
    vec4 texel = mix(texelNear, texelFar, blendFactor);

    // Monta a cor final com iluminação
    vec3 color = clamp(texel.rgb * (ambient + diffuse) + specular, 0.0, 1.0);

    //DEBUG, mostra o mapa
    //outColor = vec4(blendMap.rgb, texel.a);


    outColor = vec4(color, 1.0);
}